<?php
/**
 * @file
 * Node Assets Input filters logic.
 */

define('NODE_ASSET_PATTERN_PREFIX', '/\\[\\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)/');
define('NODE_ASSET_PATTERN', '/\\[\\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)\\s\\{((\\n|.)*?)\\}\\snode-asset:\2\\]\\]/s');

/**
 * Renders node asset with overridden optional fields.
 */
function node_assets_render_by_tag($node_asset, $params_string, $in_editor = FALSE) {

  if (!empty($node_asset)) {

    $clone = clone($node_asset);
    $clone->in_editor = $in_editor;

    if (!$in_editor) {
      // Need double decode if ckeditor plugin HTML entities is enabled.
      $params_string = html_entity_decode(html_entity_decode($params_string, ENT_QUOTES, 'UTF-8'));
    }
    $options = drupal_json_decode('{' . $params_string . '}', TRUE);
    $view_mode = NODE_ASSET_DEFAULT_MODE;

    if (is_array($options)) {
      foreach ($options as $name => $value) {
        if ($name == 'mode') {
          $view_mode = $value;
        }
        else {
          _node_assets_set_field_value($clone, $name, $value);
        }
      }
    }
    
    $clone->node_asset_options = $options;
    $clone->node_asset_source_string = $params_string;
    
    // As render works by reference we should pass variable instead of direct value.
    $clone_content = node_view($clone, $view_mode);
    $rendered_content = render($clone_content);

    return $rendered_content;
  }
  return '';
}

/**
 * Helper function to keep the initial regexp only here, returns a set of matches per node asset tag.
 */
function node_assets_filter_get_matches($text) {
  $matches = array();
  preg_match_all(
    NODE_ASSET_PATTERN,
    $text,
    $matches,
    PREG_SET_ORDER);

  return $matches;
}

/**
 * Filter callback for node assets markup filter.
 */
function node_assets_filter_process($text) {

  global $user;

  while (preg_match_all(NODE_ASSET_PATTERN_PREFIX, $text, $matches, PREG_SET_ORDER) != 0) {

    $user->node_asset['filters'][] = array();

    $text = preg_replace_callback(
      NODE_ASSET_PATTERN,
      'node_assets_filter_replace_process',
      $text
    );

    if (isset($user->node_asset['filters']) && $user->node_asset['filters'] > 0) {
      $end_filter = array_pop($user->node_asset['filters']);
    }
    if (isset($user->node_asset['uuids']) && $user->node_asset['uuids'] > 0) {
      $end_uuids = array_pop($user->node_asset['uuids']);
    }
  }

  return $text;

}

/**
 * Filter callback for remove node assets filter.
 */
function node_assets_cut_filter_process($text) {
  $matches = node_assets_filter_get_matches($text);
  $tags = array();
  foreach ($matches as $match) {
    $tags[] = $match[0];
  }
  if (count($tags)) {
    $text = str_replace($tags, '', $text);
  }
  return $text;
}

/**
 * Filter callback for node_assets_filter_process.
 */
function node_assets_filter_replace_process($matches) {

  global $user;

  if (!empty($matches[2]) && !empty($matches[1])
    && !empty($matches[3])
  ) {

    $entity_type = 'node';
    $uuids = array($matches[2]);

    $entities = entity_uuid_load($entity_type, $uuids);

    if (!empty($entities)) {
      $uuid_node = array_pop($entities);
      $node_asset = node_load($uuid_node->nid);
      
      $current_filter = key($user->node_asset['filters']);
      if (count($user->node_asset['filters']) > 1) {

        end($user->node_asset['filters']);
        $current_filter = key($user->node_asset['filters']);

        prev($user->node_asset['filters']);
        $previous_filter = key($user->node_asset['filters']);

        $previous_end = key($user->node_asset['filters'][$previous_filter]);

        if (count($user->node_asset['filters'][$previous_filter]) > 1) {

          end($user->node_asset['filters'][$previous_filter]);
          $previous_end = key($user->node_asset['filters'][$previous_filter]);

        }

        $user->node_asset['uuids'][] = $user->node_asset['filters'][$previous_filter][$previous_end];

      }

      if (!isset($user->node_asset['uuids']) || !in_array($node_asset->uuid, $user->node_asset['uuids'])) {

        $user->node_asset['filters'][$current_filter][] = $node_asset->uuid;
        $rendered = node_assets_render_by_tag($node_asset, $matches[3]);
      
        return $rendered;
      }
      else {
        $error_anchor = rand();
        $error_link = l(t('See Error'), '',  array(
          'fragment' => $error_anchor,
          'external' => TRUE,
          'attributes' => array(
            'title' => 'See Error'
          )
        ));

        watchdog('node_asset', 'The placement of Node Asset "%title" cannot be fully rendered because it causes an infinite render loop.', array('%title' => $node_asset->title), WATCHDOG_ERROR);
        drupal_set_message(t('The placement of Node Asset "%title" cannot be fully rendered because it causes an infinite render loop. ', array('%title' => $node_asset->title)) . $error_link, 'error');
        return '<div id="' . $error_anchor . '" class="node-asset-error">X</div>';
      }
    }
    else {
      $error_anchor = rand();
      $error_link = l(t('See Error'), '',  array(
        'fragment' => $error_anchor,
        'external' => TRUE,
        'attributes' => array(
          'title' => 'See Error'
        )
      ));

      watchdog('node_asset', 'There is a Node Asset placed that cannot be found in this site.', array(), WATCHDOG_ERROR);
      drupal_set_message(t('There is a Node Asset placed that cannot be found in this site. ') . $error_link, 'error');
      return '<div id="' . $error_anchor . '" class="node-asset-error">X</div>';
    }
  }

  return $matches[0];
}

/**
 * Helper function to override the optional node asset field values.
 */
function _node_assets_set_field_value(&$node_asset, $field_name, $values) {
  $values = (array) $values;
  $overridden_fields = _node_assets_get_overridable_fields($node_asset->type);

  if (isset($overridden_fields[$field_name])) {
    if (!count($values)) {
      $values = array('');
    }

    $langcode = field_language('node', $node_asset, $field_name);

    if (!$langcode) {
      $langcode = LANGUAGE_NONE;
    }

    $field_type = $overridden_fields[$field_name]['type'];
    switch ($field_type) {
      // @todo: We should check the type of widget instead.
      case 'text':
      case 'text_long':
        if (!isset($node_asset->{$field_name}[$langcode])) {
          $node_asset->{$field_name}[$langcode] = array();
        }
        foreach ($values as $delta => $value) {
          $value = html_entity_decode($value, ENT_NOQUOTES, 'UTF-8');
          if (!isset($node_asset->{$field_name}[$langcode][$delta])) {
            $node_asset->{$field_name}[$langcode][$delta] = array();
          }
          if (isset($node_asset->{$field_name}[$langcode][$delta]['format']) && $node_asset->{$field_name}[$langcode][$delta]['format']) {
            $safe_value = check_markup($value, $node_asset->{$field_name}[$langcode][$delta]['format']);
          }
          else {
            $safe_value = check_plain($value);
          }
          $node_asset->{$field_name}[$langcode][$delta]['value'] = $value;
          $node_asset->{$field_name}[$langcode][$delta]['safe'] = $safe_value;
          $node_asset->{$field_name}[$langcode][$delta]['safe_value'] = $value;
        }
        break;
    }
  }
}
