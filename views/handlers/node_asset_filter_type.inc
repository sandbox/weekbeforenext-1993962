<?php

/**
 * @file
 * Contains the group filter for field group audience - role.
 */

/**
 * Field group audience - "role" filter handler.
 */
class node_asset_filter_type extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Node Asset Type');
      $this->value_options = node_assets_get_types_list();
    }
  }

}
