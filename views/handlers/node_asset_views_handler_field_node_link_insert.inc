<?php
/**
 * @file
 * Field handler to present a library button.
 */

/**
 * Field handler to present a library button.
 *
 * @ingroup views_field_handlers
 */
class node_asset_views_handler_field_node_link_insert extends views_handler_field_node_link {

  /**
   * Return HTML.
   */
  function render_link($node_asset, $values) {
    $this->options['alter']['make_link'] = FALSE;
    $text = !empty($this->options['text']) ? $this->options['text'] : t('Add to editor');
    return '<button type="button" class="node-assets-item-button" id="node-asset-item.' . $node_asset->uuid . '.' . $node_asset->type . '">' . $text . '</button>';
  }

}
