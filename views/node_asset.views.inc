<?php

/**
 * Implements hook_views_handlers().
 */
function node_asset_views_handlers() {
  return array(
    'info' => array(
      'path' => NODE_ASSET_MODULE_PATH . '/views/handlers',
    ),
    'handlers' => array(
      'node_asset_views_handler_field_node_link_insert' => array(
        'parent' => 'views_handler_field_node_link',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function node_asset_views_data_alter(&$data) {
  $data['node']['insert_node_asset'] = array(
    'field' => array(
      'title' => t('Insert button'),
      'help' => t('Provide a button for node assets library.'),
      'handler' => 'node_asset_views_handler_field_node_link_insert',
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function node_asset_views_data() {

  $data['node_asset_type']['table']['group'] = t('Node Asset Types');
  $data['node_asset_type']['table']['join'] = array(
    'node' => array(
      'left_field' => 'type',
      'field' => 'type',
    ),
  );

  $data['node_asset_type']['type'] = array(
    'title' => t('Node Asset Content Type'),
    'help' => t('The content type of the node asset.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'type',
      'handler' => 'views_handler_relationship',
      'label' => t('Node Asset Content Type'),
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'node_asset_filter_type',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['node_asset_type']['name'] = array(
    'title' => t('Node Asset Content Type Name'),
    'help' => t('The content type name of the node asset.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['node_asset_type']['icon'] = array(
    'title' => t('Node Asset Icon'),
    'help' => t('The icon for the node asset.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['node_asset_type']['weight'] = array(
    'title' => t('Node Asset Weight'),
    'help' => t('The weight of the node asset.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
