Node Asset
-----

Introduction
------------

The Node Asset module proposes a new approach to the media management in Drupal.
Integration with CKEditor module provides the precise content management opportunity as you manipulate by fully rendered node asset representation.

INSTALLATION:
------------
Download and enable the module as usual.

Create content types that you wish to make node assets.
Click on "Node Asset settings" in the content type edit tab.
Enable the "Node Asset" checkbox and select an icon to be used in CKEditor.

Text Formats configuration
--------------------------
Configure an input format you want to use with CKEditor and node assets.
Enable "Convert node asset tags to markup" filter and set it as a first at the "Filter processing order"
Enable "Remove node asset tags from output" filter

CKEDITOR configuration
----------------------
Follow the installation instructions of the CKEditor module.
Enable the ckeditor profile for the previously configured input format.

CKEditor 4.4.3 with iframedialog, tableresize, and fakeobjects plugins is the highest compatible version.

Make the following changes on the ckeditor profile edition page:

in the "Editor appearance" fieldset enable "Node assets" checkbox in the Plugins list, choose the desired buttons
of your node asset types to be used in editor.

in the "Cleanup and output" fieldset we suggest to choose <br> "Enter mode" for now.

in the "Advanced options" fieldset set "HTML Entities" to "No" (At this moment it is a required option if you use the Node Asset module.)
