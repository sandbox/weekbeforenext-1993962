<?php
/**
 * @file
 * Node Asset wrapper template.
 */
?>
<div <?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><a
      href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php print render($content); ?></div>
