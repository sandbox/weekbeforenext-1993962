<?php
/**
 * @file
 * Template for asset wrapper in wysiwyg.
 */
?>
<div <?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php print render($content); ?></div>
