<?php
/**
 * @file
 * Node Asset module.
 */

// Defines.
define('NODE_ASSET_DEFAULT_MODE', 'full');
define('NODE_ASSET_MODULE_PATH', drupal_get_path('module', 'node_asset'));
define('NODE_ASSET_MODULE_CKEDITOR_PLUGIN_PATH', NODE_ASSET_MODULE_PATH . '/ckeditor');

// Includes.
require_once (NODE_ASSET_MODULE_PATH . '/includes/node_asset.filters.inc');

/**
 * Implements hook_preprocess_page().
 */
function node_asset_preprocess_page(&$vars) {
  drupal_add_css(NODE_ASSET_MODULE_PATH . '/css/node-assets.css');
  drupal_add_css(NODE_ASSET_MODULE_PATH . '/css/node-assets-library.css');
}

/**
 * Implements hook_page_build().
 */
function node_asset_page_build(&$page) {

  $page['page_bottom']['node_asset']['#attached']['css'][NODE_ASSET_MODULE_PATH . '/css/node-assets.css'] = array('every_page' => TRUE);
  $page['page_bottom']['node_asset']['#attached']['css'][NODE_ASSET_MODULE_PATH . '/css/node-assets-library.css'] = array('every_page' => TRUE);

}

/**
 * Implements hook_permission().
 */
function node_asset_permission() {
  return array(
    'administer node assets' => array(
      'title' => t('Administer Node Assets'),
      'description' => t('Allows users to administer node assets.'),
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function node_asset_form_node_type_form_alter(&$form, &$form_state) {
  $node_type = $form['#node_type']->type;
  $node_asset_type = FALSE;
  $node_asset_icon = '';
  $node_asset_weight = '';

  $is_node_asset_type = node_asset_is_node_asset_content_type($node_type);

  if ($is_node_asset_type) {
    $node_asset_type = TRUE;
    $node_asset_icon = $is_node_asset_type['icon'];
    $node_asset_weight = $is_node_asset_type['weight'];
  }

  $form['node_asset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Asset settings'),
    '#collapsible' => TRUE,
    '#collapsed' => !$node_asset_type,
    '#group' => 'additional_settings',
    '#description' => t('Identify this content type as a node asset and assign a representative icon for the WYSIWYG editor.'),
  );

  $form['node_asset']['node_asset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Node Asset'),
    '#default_value' => $node_asset_type,
    '#description' => t('Set this content type to be a node asset, that can be embedded using a WYSIWYG editor.'),
  );

  $icons_options = array('none' => t('No icon'));
  $module_icons = _node_asset_get_icons();
  if (!empty($module_icons)) {
    $icons_options = $icons_options + $module_icons;
  }

  $form['node_asset']['node_asset_icon'] = array(
    '#type' => 'radios',
    '#default_value' => $node_asset_icon,
    '#options' => $icons_options,
    '#title' => t('WYSIWYG Button icon'),
  );

  $form['node_asset']['node_asset_weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight',
    '#default_value' => $node_asset_weight,
    '#weight' => 50,
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function node_asset_form_field_ui_display_overview_form_alter(&$form, &$form_state) {
  if ('node' == $form['#entity_type'] && node_asset_is_node_asset_content_type($form['#bundle'])) {
    if ($form['#view_mode'] == 'default') {
      $bundle = $form['#bundle'];
      $form['wysiwyg_modes'] = array(
        '#type' => 'fieldset',
        '#title' => t('Wysiwyg modes'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $options = array();
      $entity_info = entity_get_info('node');
      $view_modes = $entity_info['view modes'];

      $default = _node_assets_get_wysiwyg_modes($bundle);
      $default_wysiwyg_default = _node_assets_get_default_wysiwyg_mode($bundle);

      foreach ($view_modes as $view_mode_name => $view_mode_info) {
        $options[$view_mode_name] = $view_mode_info['label'];
      }

      $form['wysiwyg_modes']['view_modes_wysiwyg'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Use following view modes in the wysiwyg'),
        '#options' => $options,
        '#default_value' => $default,
      );

      $form['wysiwyg_modes']['default_view_mode_wysiwyg'] = array(
        '#type' => 'radios',
        '#title' => t('Use following view mode as default in the wysiwyg'),
        '#options' => $options,
        '#default_value' => $default_wysiwyg_default,
      );

      $form['#submit'][] = 'node_assets_field_ui_display_overview_form_submit';
    }
  }
}

/**
 * Custom submit handler.
 */
function node_assets_field_ui_display_overview_form_submit($form, &$form_state) {
  $bundle = $form['#bundle'];
  $view_modes_wysiwyg = array_filter($form_state['values']['view_modes_wysiwyg']);
  node_assets_set_wysiwyg_modes($bundle, $view_modes_wysiwyg);
  $default_view_mode_wysiwyg = $form_state['values']['default_view_mode_wysiwyg'];
  node_assets_set_default_wysiwyg_mode($bundle, $default_view_mode_wysiwyg);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function node_asset_form_ckeditor_admin_profile_form_alter(&$form, &$form_state) {
  $form['output']['enter_mode']['#default_value'] = 'br';
  $form['#submit'][] = 'node_asset_ckeditor_admin_profile_form_submit';
}

/**
 * Custom submit handler.
 */
function node_asset_ckeditor_admin_profile_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['loadPlugins']['node_asset']) && ($form_state['values']['enter_mode'] != 'br')) {
    $form_state['values']['enter_mode'] = 'br';
  }
}

/**
 * Helper function to get wysiwyg modes.
 */
function _node_assets_get_wysiwyg_modes($bundle) {
  $settings = variable_get('node_assets_wysiwyg_modes', array());
  return isset($settings[$bundle]) ? $settings[$bundle] : array();
}

/**
 * Helper function to set wysiwyg modes.
 */
function node_assets_set_wysiwyg_modes($bundle, $settings) {
  $current_settings = variable_get('node_assets_wysiwyg_modes', array());
  $current_settings[$bundle] = $settings;
  variable_set('node_assets_wysiwyg_modes', $current_settings);
}

/**
 * Get default wysiwyg mode by bundle.
 */
function _node_assets_get_default_wysiwyg_mode($bundle) {
  $settings = variable_get('node_assets_default_wysiwyg_modes', array());
  return isset($settings[$bundle]) ? $settings[$bundle] : array();
}

/**
 * Set default wysiwyg mode by bundle.
 */
function node_assets_set_default_wysiwyg_mode($bundle, $settings) {
  $current_settings = variable_get('node_assets_default_wysiwyg_modes', array());
  $current_settings[$bundle] = $settings;
  variable_set('node_assets_default_wysiwyg_modes', $current_settings);
}

/**
 * Implements hook_node_type_insert().
 */
function node_asset_node_type_insert($info) {
  node_asset_node_type_save($info);
}

/**
 * Implements hook_node_type_update().
 */
function node_asset_node_type_update($info) {
  node_asset_node_type_save($info);
}

/**
 * Implements hook_node_type_delete().
 */
function node_asset_node_type_delete($info) {
  $node_type = $info->type;
  if (db_table_exists('node_asset_type')) {
    $delete = db_delete('node_asset_type')
      ->condition('type', $node_type)
      ->execute();
  }
}

/**
 * Add group and group content fields to new content types.
 */
function node_asset_node_type_save($info) {
  $node_type = $info->type;
  $node_type_name = $info->name;
  $is_node_asset_type = node_asset_is_node_asset_content_type($node_type);

  if (variable_get('node_asset_' . $info->type, FALSE) && !$is_node_asset_type) {
    $node_asset_icon = variable_get('node_asset_icon_' . $node_type);
    $node_asset_weight = variable_get('node_asset_weight_' . $node_type);
    if (db_table_exists('node_asset_type')) {
      $insert = db_insert('node_asset_type')
        ->fields(array(
          'type' => $node_type,
          'name' => $node_type_name,
          'icon' => $node_asset_icon,
          'weight' => $node_asset_weight,
        ))
        ->execute();
    }
  }

  if (variable_get('node_asset_' . $info->type, FALSE) && $is_node_asset_type) {
    $node_asset_icon = variable_get('node_asset_icon_' . $node_type);
    $node_asset_weight = variable_get('node_asset_weight_' . $node_type);
    if (db_table_exists('node_asset_type')) {
      $update = db_update('node_asset_type')
        ->fields(array(
          'type' => $node_type,
          'name' => $node_type_name,
          'icon' => $node_asset_icon,
          'weight' => $node_asset_weight,
        ))
        ->condition('type', $node_type, '=')
        ->execute();
    }
  }

  if (!variable_get('node_asset_' . $info->type, FALSE) && $is_node_asset_type) {
    $node_type = $info->type;
    if (db_table_exists('node_asset_type')) {
      $delete = db_delete('node_asset_type')
        ->condition('type', $node_type)
        ->execute();
    }
  }

  // Delete the variable, as we will rely on the presence of the field.
  $vars = array(
    'node_asset',
    'node_asset_icon',
    'node_asset_weight',
  );
  foreach ($vars as $var) {
    variable_del($var . '_' . $node_type);
  }
}

/**
 * Helper function to get the list of icons.
 */
function _node_asset_get_icons() {
  $dir = NODE_ASSET_MODULE_PATH . '/ckeditor/buttons/';
  $icons = &drupal_static(__FUNCTION__, array());

  if (!$icons) {
    if (is_dir($dir)) {

      foreach (scandir($dir) as $file_name) {
        if (!in_array($file_name, array('.', '..', '.svn'))) {
          $file_path = $dir . $file_name;

          if (FALSE !== getimagesize($file_path)) {
            $icons[$file_name] = theme('image', array(
              'path' => $file_path,
              'width' => 18,
              'height' => 18,
            ));
          }
        }
      }
    }
  }

  return $icons;
}

/**
 * Helper function to get supported field types.
 */
function _node_assets_get_supported_field_types() {
  // @todo: Provide a hook.
  return array('text', 'text_long');
}

/**
 * Helper function to check the asset field.
 *
 * Can we provide an override for its value or not.
 */
function _node_assets_get_overridable_fields($node_asset_type) {
  $supported_types = _node_assets_get_supported_field_types();
  $cache = drupal_static(__FUNCTION__, array());

  if (!isset($cache[$node_asset_type])) {
    $cache[$node_asset_type] = array();
    $fields_info = field_info_instances('node', $node_asset_type);

    foreach ($fields_info as $field_info) {
      if (!$field_info['required']) {
        $field_name = $field_info['field_name'];
        $label = $field_info['label'];
        $info = field_info_field($field_name);

        if (!empty($info) && in_array($info['type'], $supported_types)) {
          $cache[$node_asset_type][$field_name] = array(
            'type' => $info['type'],
            'label' => $label,
          );
        }
      }
    }
  }

  return $cache[$node_asset_type];
}

function node_asset_is_node_asset_content_type($node_type) {
  $result = db_select('node_asset_type', 'nat')
    ->fields('nat')
    ->condition('type', $node_type, 'LIKE')
    ->execute()
    ->fetchAssoc();
  return $result;
}

function node_assets_get_types_list() {

  $result = db_select('node_asset_type', 'nat')
    ->fields('nat', array('type', 'name'))
    ->orderBy('type', 'DESC')
    ->execute()
    ->fetchAllkeyed();

  return $result;
}

/**
 * Gets an array of all asset types, keyed by the type name.
 */
function node_assets_get_types() {
  $result = db_select('node_asset_type', 'nat')
    ->fields('nat')
    ->execute()
    ->fetchAllAssoc('type');
  return $result;
}

/**
 * Helper function to check the asset field.
 *
 * Can we provide an override for its value or not.
 */
function _node_asset_get_overridable_fields($asset_type) {
  $supported_types = _node_asset_get_supported_field_types();
  $cache = drupal_static(__FUNCTION__, array());

  if (!isset($cache[$asset_type])) {
    $cache[$asset_type] = array();
    $fields_info = field_info_instances('node-asset', $asset_type);

    foreach ($fields_info as $field_info) {
      if (!$field_info['required']) {
        $field_name = $field_info['field_name'];
        $label = $field_info['label'];
        $info = field_info_field($field_name);

        if (!empty($info) && in_array($info['type'], $supported_types)) {
          $cache[$asset_type][$field_name] = array(
            'type' => $info['type'],
            'label' => $label,
          );
        }
      }
    }
  }

  return $cache[$asset_type];
}

/**
 * Implements hook_ckeditor_plugin().
 */
function node_asset_ckeditor_plugin() {
  $plugin_name = 'nodeasset';
  $plugins = array(
    $plugin_name => array(
      'name' => $plugin_name,
      'desc' => t('Node assets'),
      'path' => NODE_ASSET_MODULE_CKEDITOR_PLUGIN_PATH . '/',
      'buttons' => array(),
    ),
  );

  $types = node_assets_get_types();
  if (!empty($types)) {
    foreach ($types as $type) {
      $plugins[$plugin_name]['buttons']['node_asset_' . $type->type] = array();
      $plugins[$plugin_name]['buttons']['node_asset_' . $type->type]['label'] = t('Node Asset') . ': ' . $type->name;
      $plugins[$plugin_name]['buttons']['node_asset_' . $type->type]['icon'] = 'buttons/' . $type->icon;
    }
  }

  if (module_exists('views')) {
    $plugins[$plugin_name]['buttons']['nodeAssetSearch'] = array(
      'label' => t('Node Assets library'),
      'icon' => 'search.png',
    );
  }

  return $plugins;
}

/**
 * Implements hook_page_alter().
 */
function node_asset_page_alter(&$page) {
  global $theme_key;

  $popup = node_asset_is_popup();

  if ($popup) {
    drupal_set_breadcrumb(array());
    module_invoke_all('suppress');

    foreach (element_children($page) as $key) {
      if ($key != 'content') {
        unset($page[$key]);
      }
    }

    // Add css in seven theme for popup.
    if ($popup) {
      $admin_theme = variable_get('admin_theme', 'seven');
      if ($theme_key == $admin_theme && ($admin_theme == 'seven')) {
        drupal_add_css(NODE_ASSET_MODULE_PATH . '/css/node-asset-popup-inner-form.css');
      }
    }
  }
}

/**
 * Helper to determine that current path used for popup.
 */
function node_asset_is_popup() {
  return isset($_GET['render']) && ($_GET['render'] == 'popup');
}

/**
 * Generates a placeholder for wysiwyg.
 */
function node_assets_get_placeholder($node_asset, array $options = array(), $override = FALSE) {
  $type = $node_asset->type;
  $tag_array = array('[[node-asset:' . $type . ':' . $node_asset->uuid . ' ');
  $fields = field_info_instances('node', $type);
  $supported_types = _node_assets_get_supported_field_types();

  if ($override) {
    foreach ($fields as $field) {
      if (!$field['required']) {
        $field_name = $field['field_name'];
        $info = field_info_field($field_name);
        $field_type = $info['type'];

        if (in_array($field_type, $supported_types)) {
          $values = field_get_items('node', $node_asset, $field_name);

          if (is_array($values)) {
            $field_values = array();
            foreach ($values as $delta => $data) {
              switch ($field_type) {
                case 'text_long':
                case 'text':
                  $field_values[$delta] = $data['value'];
                  break;
              }
            }

            $options[$field_name] = $field_values;
          }
        }
      }
    }
  }

  $tag_array[] = drupal_json_encode($options);
  $tag_array[] = ' node-asset:' . $node_asset->uuid . ']]';
  $final_tag = implode('', $tag_array);

  return $final_tag;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function node_asset_form_views_exposed_form_alter(&$form, &$form_state) {
  // Fix redirection for views exposed form.
  // In case that views_exposed_form form builder gets only view path, we should rebuild $form_state['redirect'].
  // We should pass query render = popup, to process suppression for unwanted items.
  // @see views_exposed_form().
  $destination = drupal_get_destination();

  if (node_asset_is_popup()) {
    $destination = drupal_parse_url($destination['destination']);
    $form_state['redirect'] = array(
      'path' => $destination['path'],
      'options' => array(
        'query' => array(
          'render' => $_GET['render'],
        ),
        'fragment' => $destination['fragment'],
      ),
    );
    $form['render'] = array(
      '#type' => 'hidden',
      '#value' => 'popup',
    );
  }
  else {
    $form['#action'] = '/' . $destination['destination'];
  }
}

function node_asset_preprocess_node(&$vars) {
  $args = arg();
  $current_node = array_search('node', $args);
  if (isset($args[$current_node + 1]) && $args[$current_node + 1] !== $vars['node']->nid && node_asset_is_node_asset_content_type($vars['node']->type)) {
    $element = $vars['elements'];
    $node_asset = $element['#node'];
    $view_mode = $element['#view_mode'];
    $base = 'node_asset_wrapper';

    $classes_array = $vars['classes_array'];
    $classes_array[] = drupal_html_class('node-asset');
    $classes_array[] = drupal_html_class("nid-$node_asset->nid");
    $classes_array[] = drupal_html_class('node-asset-' . $element['#bundle']);
    $classes_array[] = drupal_html_class('node-asset-' . $element['#view_mode']);
    $classes_array[] = drupal_html_class('node-asset-' . $element['#bundle'] . '-' . $element['#view_mode']);

    if (isset($node_asset->in_editor) && $node_asset->in_editor) {
      $classes_array[] = drupal_html_class('editor');
      $base = 'node_asset_editor_wrapper';
      $vars['attributes_array']['contenteditable'] = 'false';
    }

    $bundle = strtr($element['#bundle'], '-', '_');
    $vars['theme_hook_suggestions'][] = $base;
    $vars['theme_hook_suggestions'][] = $base . '__' . $bundle;
    $vars['theme_hook_suggestions'][] = $base . '__' . $view_mode;
    $vars['theme_hook_suggestions'][] = $base . '__' . $bundle . '__' . $view_mode;

    $vars['classes_array'] = $classes_array;
    $vars['attributes_array']['class'] = $classes_array;
    $vars['attributes_array']['id'] = $node_asset->type . '-' . $node_asset->nid;

    $vars['node'] = $node_asset;
    $vars['view_mode'] = $view_mode;
  }
}

/**
 * Implements hook_entity_info_alter().
 */
function node_asset_entity_info_alter(&$info) {
  $info['node']['view modes'] += array(
    'large_asset' => array(
      'label' => t('Large Node Asset'),
      'custom settings' => TRUE,
    ),
    'small_asset' => array(
      'label' => t('Small Node Asset'),
      'custom settings' => TRUE,
    ),
    'node_asset_views_result' => array(
      'label' => t('Node Asset Views Result'),
      'custom settings' => TRUE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function node_asset_theme() {
  $base = array(
    'path' => NODE_ASSET_MODULE_PATH . '/theme',
  );

  return array(
    'node_asset' => array(
      'render element' => 'elements',
      'template' => 'node-asset',
    ) + $base,
    'node_asset_wrapper' => array(
      'render element' => 'element',
      'template' => 'node-asset-wrapper',
    ) + $base,
    'node_asset_editor_wrapper' => array(
      'render element' => 'element',
      'template' => 'node-asset-editor-wrapper',
    ) + $base,
  );
}

/**
 * Implements hook_menu().
 */
function node_asset_menu() {
  $items = array();

  $items['admin/node-assets/override'] = array(
    'title' => 'Override node asset',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_assets_override_form'),
    'access arguments' => array('administer node assets'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/node-assets/get'] = array(
    'theme callback' => 'node_asset_get_frontend_theme',
    'page callback' => 'node_assets_get_content',
    'page arguments' => array(3),
    'access arguments' => array('administer node assets'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/node-assets/getfull'] = array(
    'theme callback' => 'node_asset_get_frontend_theme',
    'page callback' => 'node_asset_get_full_content',
    'page arguments' => array(3),
    'access arguments' => array('administer node assets'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/node-assets/tag/%/%'] = array(
    'theme callback' => 'node_asset_get_frontend_theme',
    'page callback' => '_node_asset_get_tag',
    'page arguments' => array(3, 4, 5),
    'access arguments' => array('administer node assets'),
    'type' => MENU_CALLBACK,
  );

  $items['node-assets-uuid'] = array(
    'title' => 'Node Asset UUID redirector with edit',
    'description' => 'Redirects requests for UUID URIs to the referenced entity edit page.',
    'page callback' => 'node_asset_uuid_redirector_edit',
    'access arguments' => array('administer node assets'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Page callback to get asset html.
 */
function node_asset_view_asset($node_asset, $view_mode = NODE_ASSET_DEFAULT_MODE) {
  $output = node_view($node_asset, $view_mode);
  return render($output);
}

/**
 * Delivery callback for HTML rendering.
 */
function node_asset_html_delivery($page_callback_result) {
  // Check if header is set.
  if (is_null(drupal_get_http_header('Content-Type'))) {
    drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
  }

  print $page_callback_result;
}

/**
 * Implements hook_form_alter().
 */
function node_asset_form_alter(&$form, &$form_state, $form_id) {

  if (isset($_GET['render']) && $_GET['render'] == 'popup' && stripos($form_id, 'node_form') !== FALSE) {

    $options = array('mode' => '');

    if (arg(2) == 'edit') {
      $options['mode'] = check_plain($_GET['mode']);
    }

    _node_assets_wysiwyg_fields($form, $options);

    if (arg(1) == 'add') {
      $form['actions']['submit']['#submit'][] = 'node_assets_wysiwyg_add_form_submit';
    }
    else {
      $form['actions']['submit']['#submit'][] = 'node_assets_wysiwyg_form_submit';
    }
    unset($form['actions']['delete']);
  }
}

/**
 * Page callback for asset override form in wysiwyg.
 */
function node_assets_override_form($form, &$form_state) {
  $form = array();

  if (isset($_REQUEST['tag']) && !empty($_REQUEST['tag'])) {
    $tag = $_REQUEST['tag'];
    $matches = node_assets_filter_get_matches($tag);

    if (!empty($matches)) {
      $match = reset($matches);
      $id = $match[2];
      $params = $match[3];

      $entity_type = 'node';
      $uuids = array($id);

      $node = entity_uuid_load($entity_type, $uuids);
      $node_asset = array_pop($node);

      $form_state['node'] = $node_asset;
      $form_state['entity_type'] = 'node';

      $form_state['build_info']['base_form_id'] = $node_asset->type . '_node_form';
      $params = '{' . $params . '}';
      $options = json_decode($params, TRUE);

      if (is_array($options)) {
        $values = array_diff_key($options, array('mode' => 1));
        foreach ($values as $name => $value) {
          _node_assets_set_field_value($node_asset, $name, $value);
        }
      }
      else {
        $options = array();
      }

      field_attach_form('node', $node_asset, $form, $form_state);
      _node_assets_clean_fields($form, $options);
      _node_assets_wysiwyg_fields($form, $options);

      $form['actions'] = array('#type' => 'actions');
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Override'),
        '#weight' => 5,
        '#submit' => array('node_assets_override_form_submit'),

      );
    }
  }
  // @todo: Add validation handlers.
  return $form;
}

/**
 * Custom submit handler.
 */
function node_assets_override_form_submit(&$form, &$form_state) {
  global $base_url;
  module_load_include('inc', 'entity', 'includes/entity.ui');
  $values = $form_state['values'];

  $view_mode = isset($values['view_mode']) ? $values['view_mode'] : NODE_ASSET_DEFAULT_MODE;

  $entity_type = 'node';
  $uuids = array($form_state['node']->uuid);

  $node = entity_uuid_load($entity_type, $uuids);
  $node_asset = array_pop($node);

  field_attach_submit('node', $node_asset, $form, $form_state);
  $placeholder = node_assets_get_placeholder($node_asset, array('mode' => $view_mode), TRUE);
  $placeholder = addslashes($placeholder);
  $placeholder = str_replace("\r\n", '\n', $placeholder);
  $editor_css_path = $base_url . '/' . drupal_get_path('module', 'node_asset') . '/ckeditor/node-assets-editor.css';
  $js = 'var CKEDITOR = parent.CKEDITOR;
    var dialog =  CKEDITOR.dialog.getCurrent();
    var outdated = dialog._outdatedNodeAssetEl;
    if (outdated) {
      var tag_id = outdated.data(\'node-asset-cid\');
      dialog._.editor.plugins.nodeasset.replaceNodeAsset(tag_id, \'' . $placeholder . '\' );
      if (CKEDITOR.env.gecko) {
        var wysiwyg = dialog._.editor.getMode();
        wysiwyg.loadData(wysiwyg.getData());
        dialog._.editor.document.appendStyleSheet(\'' . $editor_css_path . '\');
      }
    }
    dialog.hide();';

  echo '<script>' . $js . '</script>';
  drupal_exit();
}

/**
 * Custom submit handler.
 */
function node_assets_wysiwyg_add_form_submit(&$form, &$form_state) {
  $node_asset = $form_state['node'];
  $values = $form_state['values'];
  $view_mode = isset($values['view_mode']) ? $values['view_mode'] : NODE_ASSET_DEFAULT_MODE;

  $placeholder = node_assets_get_placeholder($node_asset, array('mode' => $view_mode));
  $placeholder = addslashes($placeholder);
  $placeholder = str_replace("\r\n", '\n', $placeholder);

  $js = 'var CKEDITOR = parent.CKEDITOR;
    var dialog = CKEDITOR.dialog.getCurrent();
    var newHtml = dialog._.editor.plugins.nodeasset.NodeAssets.getContentByTag(\'' . $placeholder . '\');
    //var newHtml = dialog.definition.NodeAssets.getContentByTag(\'' . $placeholder . '\');
    if (newHtml) {
      var element = CKEDITOR.dom.element.createFromHtml(newHtml);
      element.setAttribute(\'contentEditable\', \'false\');   //Hello, Chrome
      dialog._.editor.insertElement(element);
      if (CKEDITOR.env.gecko && newHtml.search(/<object /i) > 0) {
        var wysiwyg = dialog._.editor.getMode();
        wysiwyg.loadData(wysiwyg.getData());
      }
    }
    dialog.hide();';

  echo '<script>' . $js . '</script>';
  drupal_exit();
}

/**
 * Custom submit handler.
 */
function node_assets_wysiwyg_form_submit(&$form, &$form_state) {
  global $base_url;
  $node_asset = $form_state['node'];
  $values = $form_state['values'];
  $view_mode = isset($values['view_mode']) ? $values['view_mode'] : NODE_ASSET_DEFAULT_MODE;

  $placeholder = node_assets_get_placeholder($node_asset, array('mode' => $view_mode));
  $placeholder = addslashes($placeholder);
  $placeholder = str_replace("\r\n", '\n', $placeholder);
  $editor_css_path = $base_url . '/' . drupal_get_path('module', 'node_asset') . '/ckeditor/node-assets-editor.css';
  $js = 'var CKEDITOR = parent.CKEDITOR;
    var dialog =  CKEDITOR.dialog.getCurrent();
    var outdated = dialog._outdatedNodeAssetEl;
    if (outdated) {
      var tag_id = outdated.data(\'node-asset-cid\');
      dialog._.editor.plugins.nodeasset.replaceNodeAsset(tag_id, \'' . $placeholder . '\' );
      if (CKEDITOR.env.gecko) {
        var wysiwyg = dialog._.editor.getMode();
        wysiwyg.loadData(wysiwyg.getData());
        dialog._.editor.document.appendStyleSheet(\'' . $editor_css_path . '\');
      }
    }
    dialog.hide();';

  echo '<script>' . $js . '</script>';
  drupal_exit();
}

/**
 * Helper function.
 */
function _node_assets_clean_fields(&$form, $options = array()) {
  $containers = element_children($form);
  $supported_types = _node_assets_get_supported_field_types();

  foreach ($containers as $key) {
    if (isset($form[$key]) && $form[$key]) {
      $elements = $form[$key][$form[$key]['#language']];
      $multiple = isset($elements['#cardinality']) && ($elements['#cardinality'] > 1 || $elements['#cardinality']) == FIELD_CARDINALITY_UNLIMITED;
      $children = element_children($elements);

      if (!empty($children)) {
        foreach ($children as $delta) {
          if (isset($elements[$delta]) && $elements[$delta]) {
            $info = field_info_field($key);
            if (!empty($info) && in_array($info['type'], $supported_types)) {
              if (isset($field)) {
                unset($field);
              }
              switch ($info['type']) {
                case 'text':
                  $field = &$form[$key][$form[$key]['#language']][$delta]['value'];
                case 'text_long':
                  $field = &$form[$key][$form[$key]['#language']][$delta];
                  break;
              }

              if (isset($field) && !$field['#required']) {
                if (isset($options[$key])) {
                  $value = (array) $options[$key];

                  if (!empty($value[$delta])) {
                    continue;
                  }
                }

                continue;
              }
            }

            unset($form[$key]);
          }
        }
      }
      else {
        $info = field_info_field($key);
        if (!empty($info) && in_array($info['type'], $supported_types)) {
          unset($field);

          switch ($info['type']) {
            case 'text':
              $field = &$form[$key][$form[$key]['#language']];
              if ($multiple) {
                $field = &$form[$key][$form[$key]['#language']]['value'];
              }
              break;
          }

          if (isset($field) && !$field['#required']) {
            if (isset($options[$key])) {
              $value = (array) $options[$key];

              if (!empty($value[$delta])) {
                continue;
              }
            }

            $field['#default_value'] = '';
            continue;
          }
        }

        unset($form[$key]);
      }
    }
  }
}

/**
 * Adds a fields for an asset tag values to the given form.
 */
function _node_assets_wysiwyg_fields(&$form, $options) {
  $entity_info = entity_get_info('node');
  $mode = isset($options['mode']) ? $options['mode'] : '';

  $modes = array();
  $modes_count = 0;

  // To get the human titles.
  $view_modes = $entity_info['view modes'];
  $type = isset($form['type']) ? $form['type']['#value'] : $form['#entity']->type;
  $wysiwyg_modes = _node_assets_get_wysiwyg_modes($type);

  if (!empty($wysiwyg_modes)) {
    foreach ($wysiwyg_modes as $view_mode_name) {
      $modes[$view_mode_name] = $view_modes[$view_mode_name]['label'];
    }

    $modes_count = count($modes);

    if (!array_key_exists($mode, $modes) && $modes_count) {
      $mode = NODE_ASSET_DEFAULT_MODE;
    }

    if ($modes_count > 1) {
      $form['view_mode_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Node Asset Display Mode'),
        '#collapsible' => FALSE,
        '#weight' => -999,
      );
      $form['view_mode_fieldset']['view_mode'] = array(
        '#type' => 'select',
        '#title' => t('Display mode'),
        '#default_value' => $mode,
        '#options' => $modes,
      );
    }
    else {
      $form['view_mode_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Display Mode'),
        '#collapsible' => FALSE,
        '#weight' => -999,
      );
      $form['view_mode_fieldset']['view_mode'] = array(
        '#type' => 'value',
        '#value' => $mode,
      );
    }
  }

  return $form;
}

/**
 * Implements hook_filter_info().
 */
function node_asset_filter_info() {
  $filters['node_assets_filter'] = array(
    'title' => t('Convert node asset tags to markup'),
    'description' => t('This filter will convert [[node-asset:...]] tags into markup'),
    'process callback' => 'node_assets_filter_process',
    'cache' => FALSE,
  );

  $filters['node_assets_cut_filter'] = array(
    'title' => t('Remove node asset tags from output'),
    'description' => t('This filter will remove [[node-asset:...]] from output'),
    'process callback' => 'node_assets_cut_filter_process',
    'cache' => TRUE,
  );

  return $filters;
}

/**
 * Implements hook_element_info_alter().
 */
function node_asset_element_info_alter(&$types) {
  $types['text_format']['#pre_render'][] = 'node_assets_pre_render_text_format';
}

/**
 * This function adds settings required by node assets.
 */
function node_assets_pre_render_text_format($element) {
  static $init = FALSE;

  if (isset($element['#format']) && ($init === FALSE)) {
    $init = TRUE;
    $node_asset_types = node_assets_get_types();
    $entity_info = entity_get_info('node');
    $view_modes = $entity_info['view modes'];
    $config = array();

    foreach ($node_asset_types as $type) {
      $node_asset_type_params = array();
      $wysiwyg_modes = _node_assets_get_wysiwyg_modes($type->type);

      $modes = array();
      foreach ($wysiwyg_modes as $view_mode_name) {
        $modes[$view_mode_name] = $view_modes[$view_mode_name]['label'];
      }

      $node_asset_type_params['name'] = $type->name;
      $node_asset_type_params['icon'] = $type->icon;
      $node_asset_type_params['fields'] = _node_assets_get_overridable_fields($type->type);
      $node_asset_type_params['modes'] = $modes;
      $config[$type->type] = $node_asset_type_params;
    }

    drupal_add_js(array('ckeditor' => array('plugins' => array('nodeasset' => $config))), 'setting');
  }

  return $element;
}

/**
 * Page callback to get html for asset preview in wysiwyg.
 */
function node_assets_get_content() {
  $content = '';

  if (isset($_REQUEST['tag']) && !empty($_REQUEST['tag'])) {
    $tag = $_REQUEST['tag'];
    $matches = node_assets_filter_get_matches($tag);
    if (!empty($matches)) {
      $match = reset($matches);

      $entity_type = 'node';
      $uuids = array($match[2]);

      $node = entity_uuid_load($entity_type, $uuids);
      $node_asset = array_pop($node);

      $content .= node_assets_render_by_tag($node_asset, $match[3], TRUE);
    }
  }

  print $content;
  drupal_exit();
}

/**
 * Page callback, return html of full asset.
 */
function node_asset_get_full_content() {
  $content = '';

  if (isset($_REQUEST['tag']) && !empty($_REQUEST['tag'])) {
    $tag = $_REQUEST['tag'];
    $matches = node_assets_filter_get_matches($tag);

    if (!empty($matches)) {
      $match = reset($matches);

      $entity_type = 'node';
      $uuids = array($match[2]);

      $node = entity_uuid_load($entity_type, $uuids);
      $node_asset = array_pop($node);

      $content .= node_assets_render_by_tag($node_asset, $match[3], FALSE);
    }
  }

  return $content;
}

/**
 * Page callback, return html of node asset by wysiwyg view mode.
 */
function _node_asset_get_tag($tag_id, $view_mode = NULL) {
  $output = '';
  $placeholder = '';
  $tags = explode(':', $tag_id);

  $entity_type = 'node';
  $uuids = array($tags[0]);

  $node = !empty($tags[0]) ? entity_uuid_load($entity_type, $uuids) : NULL;

  if ($node) {

    $node_asset = array_pop($node);
    if (empty($view_mode) || ($view_mode == 'default')) {
      $view_modes = variable_get('node_assets_default_wysiwyg_modes', array());
      if (!empty($view_modes[$node_asset->type])) {
        $view_mode = $view_modes[$node_asset->type];
      }
      else {
        $view_mode = NODE_ASSET_DEFAULT_MODE;
      }
    }

    $node_asset->in_editor = TRUE;
    $placeholder = node_assets_get_placeholder(
      $node_asset,
      array(
        'mode' => $view_mode,
      ),
      FALSE
    );
    $placeholder = str_replace("\r\n", '\n', $placeholder);

    $node_asset_content = node_view($node_asset, $view_mode);
    $output = render($node_asset_content);
  }

  print drupal_json_encode(
    array(
      'tag' => $placeholder,
      'content' => $output,
    )
  );

  drupal_exit();
}

/**
 * Redirects all UUID URI requests to the appropriate entity edit page.
 */
function node_asset_uuid_redirector_edit() {
  $options = array();
  $entity_data = uuid_uri_array_to_data(arg());

  $entity_info = entity_get_info($entity_data['entity_type']);
  if (empty($entity_info['uuid'])) {
    return drupal_not_found();
  }

  $entities = entity_uuid_load($entity_data['entity_type'], array($entity_data['uuid']));
  if (!count($entities)) {
    return drupal_not_found();
  }

  $uri = entity_uri($entity_data['entity_type'], current($entities));

  if (count($_GET) > 1) {
    $options['query']['render'] = check_plain($_GET['render']);
    $options['query']['mode'] = check_plain($_GET['mode']);
  }

  $path = $uri['path'] . '/edit';
  drupal_goto($path, $options, 301);
}

/**
 * Implements hook_views_api().
 */
function node_asset_views_api() {
  return array(
    'api' => '3.0',
    'path' => NODE_ASSET_MODULE_PATH . '/views',
  );
}
