<?php

/**
 * Menu callback: displays the search module settings page.
 *
 * @ingroup forms
 *
 * @see node_asset_index_admin_settings_validate()
 * @see node_asset_index_admin_settings_submit()
 * @see node_asset_index_admin_reindex_submit()
 */
function node_asset_index_admin_settings_form($form) {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );
  // @todo node_type_get_names() can return an empty array!?
  $names = node_type_get_names();
  $form['settings']['node_asset_index_scan_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Scan content types for node assets'),
    '#default_value' => variable_get('node_asset_index_scan_nodetypes', array()),
    '#options' => array_map('check_plain', $names),
    '#description' => t('Enable node asset indexing for the selected content type(s).'),
  );

  $items = drupal_map_assoc(array(10, 20, 50, 100, 200, 500));

  // Indexing throttle:
  $form['indexing_throttle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Indexing throttle'),
  );
  $form['indexing_throttle']['node_asset_index_cron_limit'] = array(
    '#type' => 'select',
    '#title' => t('Number of items to index per batch run'),
    '#default_value' => variable_get('node_asset_index_cron_limit', 100),
    '#options' => $items,
    '#description' => t('The maximum number of items indexed in each pass of a batch process. If necessary, reduce the number of items to prevent timeouts and memory errors while indexing.'),
  );

  // Buttons are only required for testing and debugging reasons.
  $description = t('This actions will clear the node asset index table in the database and analyze all selected content types (see settings above) for new/updated/removed node assets. Use this only for immediate cleanup tasks and to force a full build or re-build of the node asset index tables.');

  $form['clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Maintenance'),
    '#description' => $description,
    '#collapsible' => FALSE,
  );
  $form['clear']['node_asset_index_clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear node asset index data and analyze content for node assets'),
    '#submit' => array('node_asset_index_clear_submit'),
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Implements hook_admin_settings_form_submit().
 */
function node_asset_index_admin_settings_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  unset($form_state['values']['node_asset_index_clear']);

  // Save form settings.
  system_settings_form_submit($form, $form_state);

  // Re-scan items, if node types or comment or custom block selection has been
  // changed.
  $additional_nodetypes_selected = array_diff($form_state['values']['node_asset_index_scan_nodetypes'], $form['settings']['node_asset_index_scan_nodetypes']['#default_value']);
  if (!empty($additional_nodetypes_selected)) {
    $node_types = array_keys(array_filter($form_state['values']['node_asset_index_scan_nodetypes']));

    // If one or more node types have been selected.
    if (!empty($node_types)) {
      module_load_include('inc', 'node_asset_index', 'node_asset_index.batch');
      batch_set(_node_asset_index_batch_scan_nodes($node_types));
    }
  }
}

/**
 * Submit callback.
 *
 * Clear link data and analyze fields in all content types.
 */
function node_asset_index_clear_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  unset($form_state['values']['node_asset_index_clear']);

  // Save form settings.
  system_settings_form_submit($form, $form_state);

  db_truncate('node_asset_index')->execute();

  // Start batch and analyze all nodes.
  $node_types = array_keys(array_filter(variable_get('node_asset_index_scan_nodetypes', array())));
  if (!empty($node_types)) {
    module_load_include('inc', 'node_asset_index', 'node_asset_index.batch');
    batch_set(_node_asset_index_batch_scan_nodes($node_types));
  }
}
