<?php

/**
 * @file
 * Batch API callbacks for the node asset index module.
 */

/**
 * Batch: Scan nodes for node assets.
 */
function _node_asset_index_batch_scan_nodes($node_types = array()) {
  // Get all active {node}.nid's.
  $result = db_query('SELECT n.nid FROM {node} n WHERE n.status = :status AND n.type IN (:types) ORDER BY n.nid', array(
    ':status' => 1,
    ':types' => $node_types,
  ));

  $operations = array();
  foreach ($result as $row) {
    $operations[] = array(
      '_node_asset_index_batch_node_scan_op',
      array($row->nid),
    );
  }
  $batch = array(
    'file' => drupal_get_path('module', 'node_asset_index') . '/node_asset_index.batch.inc',
    'finished' => '_node_asset_index_batch_node_scan_finished',
    'operations' => $operations,
    'title' => t('Scanning for node assets'),
  );

  return $batch;
}

/**
 * Batch operation: Scan one by one node for node assets.
 */
function _node_asset_index_batch_node_scan_op($nid, &$context) {
  // Load the node and scan for links.
  $node = node_load($nid, NULL, TRUE);
  _node_asset_index_add_node_assets($node);

  // Store results for post-processing in the finished callback.
  $context['results'][] = $node->nid;
  $context['message'] = t('Content: @title', array('@title' => $node->title));
}

/**
 * Output node batch result messages.
 *
 * @param bool $success
 *   If scan completed successfully or not.
 * @param int $results
 *   Number of nodes scanned.
 * @param array $operations
 *   Array of functions called.
 */
function _node_asset_index_batch_node_scan_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One node has been scanned.', '@count nodes have been scanned.');
  }
  else {
    $message = t('Scanning for node assets in content has failed with an error.');
  }
  drupal_set_message($message);
}

function _node_asset_index_batch_scan_single_node($nid, $missing_assets_count) {
  $operations = array();
  $assets_per_run = variable_get('node_asset_index_cron_limit', 100);
  for ($i = 0; $i <= $missing_assets_count; $i = $i + $assets_per_run) {
    $operations[] = array(
      '_node_asset_index_batch_single_node_scan_op',
      array($nid),
    );
  }
  $batch = array(
    'file' => drupal_get_path('module', 'node_asset_index') . '/node_asset_index.batch.inc',
    'finished' => '_node_asset_index_batch_single_node_scan_finished',
    'operations' => $operations,
    'title' => t('Scanning for node assets'),
    'progress_message' => t('Remaining @remaining of @total scans.'),
  );

  return $batch;
}

/**
 * Run single node asset extraction.
 *
 * @param int $nid
 *   Node ID.
 * @param array $context
 *   Batch context array.
 */
function _node_asset_index_batch_single_node_scan_op($nid, &$context) {
  // Load the node and scan for node assets.
  $node = node_load($nid, NULL, TRUE);
  _node_asset_index_add_node_assets($node, TRUE);

  // Store results for post-processing in the finished callback.
  $context['results'][] = $node->nid;
  $context['message'] = t('Content: @title', array('@title' => $node->title));
}

/**
 * Output single node batch result messages.
 *
 * @param bool $success
 *   If scan completed successfully or not.
 * @param int $results
 *   How often the node has been scanned.
 * @param array $operations
 *   Array of functions called.
 */
function _node_asset_index_batch_single_node_scan_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'Node @nid has been re-scanned once to collect all node assets.', 'Node @nid has been re-scanned @count times to collect all node assets.', array('@nid' => $results[0]));
  }
  else {
    $message = t('Recurring scanning for node assets in node @nid has failed with an error.', array('@nid' => $results[0]));
  }
  drupal_set_message($message);
}
