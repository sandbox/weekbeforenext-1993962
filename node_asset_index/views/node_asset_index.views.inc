<?php

/**
 * Implements hook_views_data().
 */
function node_asset_index_views_data() {

  $data['node_asset_index']['table']['group'] = t('Node Asset Index');
  $data['node_asset_index']['table']['join'] = array(
    'node' => array(
      'left_field' => 'uuid',
      'field' => 'uuid',
    ),
  );
  $data['node_asset_index']['table']['join'] = array(
    'node' => array(
      'left_field' => 'uuid',
      'field' => 'nauuid',
    ),
  );

  $data['node_asset_index']['uuid'] = array(
    'title' => t('Node UUID'),
    'help' => t('The node ID of the scanned node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'uuid',
      'handler' => 'views_handler_relationship',
      'label' => t('Scanned Node'),
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['node_asset_index']['nauuid'] = array(
    'title' => t('Node Asset UUID'),
    'help' => t('The node ID of the asset found in the scanned node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'uuid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node Asset'),
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
