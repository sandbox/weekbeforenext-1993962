CKEDITOR.plugins.setLang('nodeasset', 'en', {
  node_assets_btn_search: 'Node Asset Search',
  node_assets_override: 'Override Node Asset',
  node_assets_edit: 'Edit Node Asset',
  node_assets_delete: 'Remove Node Asset',
  node_assets_cut: 'Cut Node Asset',
  node_assets_paste: 'Paste Node Asset',
  node_assets_error_paste: 'Error occurred during paste operation.',
  node_assets_nl_before: 'New Line Above',
  node_assets_nl_after: 'New Line Below'
});
