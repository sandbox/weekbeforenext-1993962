/**
 * @file
 * NodeAsset plugin for ckeditor.
 */

var NodeAssets;
(function ($) {
  // If for some reason buttons should not be accessible - remove them from rendering.
  if (Drupal.settings.ckeditor
    && Drupal.settings.ckeditor.plugins
    && Drupal.settings.ckeditor.plugins.asset
    && Drupal.settings.ckeditor.plugins.asset.removeButtons) {
    var removeButtons = Drupal.settings.ckeditor.plugins.asset.removeButtons.join(',');
    CKEDITOR.config.removeButtons = CKEDITOR.config.removeButtons
      ? CKEDITOR.config.removeButtons + ',' + removeButtons : removeButtons;
  }

  // Temporary container for node asset html.
  var tempContainer = document.createElement('DIV'),
    tagCache = {},
    cutted = null;

  // Node Assets object.
  NodeAssets = {
    selectedElement: null,

    getCKeditorVersion: function () {
      if (CKEDITOR.version) {
        var explodedVersion = CKEDITOR.version.split('.');
        return explodedVersion[0] ? parseInt(explodedVersion[0]) : null;
      }
    },

    getTranslatedString: function (editor, stringKey) {
      return (NodeAssets.getCKeditorVersion() >= 4) ? editor.lang.nodeasset[stringKey] : editor.lang[stringKey];
    },

    insertAssetInEditor: function (editor, element) {
      // If we are dropping directly on other selected Asset insert right after it.
      if (NodeAssets.selectedElement) {
        var selection = editor.getSelection();
        var selectedElement = NodeAssets.selectedElement;
        NodeAssets.deselect();
        var newline = NodeAssets.getEnterElement(editor);
        newline.insertAfter(selectedElement);
        selection.selectElement(newline);
      }
      editor.insertElement(element);
    },

    removeAssetFromEditor: function (editor, element, cut) {
      if (element) {
        if (cut) {
          cutted = element;
        }
        NodeAssets.deselect();
        var range = editor.getSelection().getRanges()[0];
        if (!range) {
          range = new CKEDITOR.dom.range(editor.document);
        }
        range.moveToPosition(element, CKEDITOR.POSITION_BEFORE_START);
        element.remove();
        range.select();
      }
    },

    select: function (element) {
      this.deselect();
      this.selectedElement = element;
      this.selectedElement.addClass('selected');
    },

    deselect: function () {
      var element = null, removeSelectedClass = function (el) {
        var cl, i, cl_arr;
        if (el) {
          if (el.removeClass) {
            el.removeClass('selected');
          }
          else {
            if (el.attributes && el.attributes['class']) {
              cl = el.attributes['class'];
              cl_arr = cl.split(' ');
              cl = [];

              for (i = 0; i < cl_arr.length; i = i + 1) {
                if (cl_arr[i] !== 'selected') {
                  cl.push(cl_arr[i]);
                }
              }

              el.attributes['class'] = cl.join(' ');
            }
          }
        }
        return el;
      };

      if (arguments.length && arguments[0]) {
        element = removeSelectedClass(arguments[0]);
      }

      if (this.selectedElement) {
        removeSelectedClass(this.selectedElement);
        this.selectedElement = null;
      }

      return element;
    },

    getSelected: function (editor) {
      if (this.selectedElement) {
        return this.selectedElement;
      }

      var range = editor.getSelection().getRanges()[0];
      if (!range) {
        return false;
      }
      range.shrink(CKEDITOR.SHRINK_TEXT);

      if (range.startContainer) {
        var node = range.startContainer;

        while (node && !(node.type === CKEDITOR.NODE_ELEMENT && node.data('node-asset-cid'))) {
          node = node.getParent();
        }

        return node;
      }
    },

    parseId: function (tag_id) {
      var arr = tag_id.split(':'), obj = {'uuid': arr[0], 'type': arr[1], 'hash': arr[2]};
      return arguments.length > 1 ? obj[arguments[1]] : obj;
    },

    generateId: function (tag_id) {
      var tagObj = this.parseId(tag_id), time = new Date().getTime();
      return [tagObj.uuid, tagObj.type, time].join(':');
    },

    getTagData: function (tag) {
      var params = {};
      var matches = tag.match(/\[\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)\s\{((\n|.)*?)\}\snode-asset:\2\]\]/);

      if (matches) {
        var paramsString = matches[3];
        paramsString = '{' + paramsString + '}';

        try {
          params = JSON.parse(paramsString);
          params.uuid = matches[2];
          params.type = matches[1];

          if (!params.mode) {
            params.mode = 'large_asset';
          }
        }
        catch (err) {
          // Empty error handler.
        }
      }

      return params;
    },

    dialog: function (editor, type) {
      return function () {
        return {
          title: 'Node Assets',
          minWidth: 800,
          minHeight: 600,
          contents: [
            {
              id: 'node_asset_frame',
              label: 'Add a node asset',
              expand: true,
              elements: [
                {
                  type: 'iframe',
                  src: Drupal.settings.basePath + 'admin/node-assets/add/' + type + '/?render=popup',
                  width: '100%',
                  height: '100%'
                }
              ]
            }
          ],
          buttons: [CKEDITOR.dialog.cancelButton]
        };
      };
    },

    adjustDialogHeight: function () {
      // CKeditor 4 have bug into plugins/dialog/plugin.js line 1036.
      // Developers forgot to add height:100% into iframe wrapper.
      // In CKeditor 3 this code present.
      setTimeout(function () {
        $('.cke_dialog_contents .cke_dialog_ui_vbox.cke_dialog_page_contents').css('height', '100%');
      }, 0);
    },

    openDialog: function (editor, dialogName, src, element) {
      editor.openDialog(dialogName, function () {
        this.definition.contents[0].elements[0].src = src;

        if (typeof(element) !== 'undefined') {
          this._outdatedNodeAssetEl = element;
        }

        // Fix height iframe wrapper issue with ckeditor 4.
        NodeAssets.adjustDialogHeight();
      });
    },

    searchDialog: function () {
      return {
        title: 'Node Assets',
        minWidth: 1000,
        minHeight: 600,
        contents: [
          {
            id: 'node_asset_frame',
            label: 'Choose a node asset from the library',
            expand: true,
            elements: [
              {
                type: 'iframe',
                src: Drupal.settings.basePath + 'admin/node-assets/search?render=popup',
                width: '100%',
                height: '100%',
                id: 'node_asset_frame_iframe',

                onContentLoad: function () {
                  $(this.getElement().$.contentDocument.body).click(
                    function (event) {
                      var target = event.target, dialog, element, wysiwyg, html;

                      if ($(target).hasClass('node-assets-item-button')) {
                        var id_arr = target.id.substr(16).split('.'),
                          uuid = (id_arr.shift()),
                          type = (id_arr).join('-'),
                          tag_id = [uuid, type, new Date().getTime()].join(':');

                        html = NodeAssets.getDataById(tag_id);
                        dialog = CKEDITOR.dialog.getCurrent();

                        if (html) {
                          element = CKEDITOR.dom.element.createFromHtml(html);
                          var editor = dialog.getParentEditor();
                          NodeAssets.insertAssetInEditor(editor, element);

                          if (CKEDITOR.env.gecko && html.search(/<object /i) > 0) {
                            wysiwyg = editor.getMode();
                            wysiwyg.loadData(wysiwyg.getData());
                          }
                        }

                        dialog.hide();
                      }
                    }
                  );
                }
              }
            ]
          }
        ],
        buttons: [CKEDITOR.dialog.cancelButton]
      };
    },

    getContainer: function (tagId, tag, content) {
      if (tagId && tag && content) {
        var $tempContainer = $(tempContainer);
        $tempContainer.html(content);

        var $node_asset_div = $tempContainer.children();
        var params = this.getTagData(tag);

        if ($node_asset_div.size()) {
          $node_asset_div.attr('data-node-asset-cid', tagId);
        }
        return tempContainer;
      }
    },

    cache: function (tagId, tag, content) {
      var html = '';
      var container = this.getContainer(tagId, tag, content);
      if (container) {
        html = container.innerHTML;
        tagCache[tagId] = {tag: tag, html: html};
        container.innerHTML = '';
      }
      return html;
    },

    getContentByTag: function (tag) {
      var content = '', tagmatches = [], time, tagId;
      $.ajax({
        type: "POST",
        url: Drupal.settings.basePath + 'admin/node-assets/get',
        data: {tag: tag},
        async: false,
        success: function (node_asset_content) {
          if (typeof(node_asset_content) == null) {
            content = '';
          }
          else {
            content = node_asset_content;
          }
        }
      });

      tagmatches = tag.match(/\[\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)\s\{((.)*?)\}\snode-asset:\2\]\]/);
      time = new Date().getTime();
      tagId = tagmatches[2] + ':' + tagmatches[1] + ':' + time;
      return this.cache(tagId, tag, content);
    },

    getDataById: function (tagId, viewMode) {
      if (typeof(tagId) != 'undefined') {
        if (typeof(viewMode) == 'undefined') {
          viewMode = 'default';
        }

        var tag = '', content = '';
        $.ajax({
          type: "POST",
          dataType: "json",
          url: Drupal.settings.basePath + 'admin/node-assets/tag/' + tagId + '/' + viewMode,
          async: false,
          success: function (data) {
            tag = data.tag.replace(/\\"/g, '"');
            content = data.content;
          }
        });

        return this.cache(tagId, tag, content);
      }
    },

    attach: function (content) {
      while (content.match(/\[\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)\s\{((.)*?)\}\snode-asset:\2\]\]/g) != null) {

        var matches = content.match(/\[\[node-asset:([_a-zA-Z0-9]+):([_a-zA-Z0-9\-]+)\s\{((.)*?)\}\snode-asset:\2\]\]/g),
          tag, im, clean_tag, html = '', cid;

        if (matches) {
          for (im = 0; im < matches.length; im = im + 1) {
            html = '';
            tag = matches[im];
            // @todo: Check that it works, needed because wysiwyg encodes 2 times.
            clean_tag = tag.replace(/&amp;quot;/g, '"');

            // Get from cache.
            for (cid in tagCache) {
              if (tagCache.hasOwnProperty(cid)) {
                if (clean_tag === tagCache[cid].tag) {
                  html = this.cache(this.generateId(cid), clean_tag, tagCache[cid].html);
                  break;
                }
              }
            }

            // Otherwise get content using ajax and cache it.
            if (!html) {
              html = this.getContentByTag(clean_tag);
            }

            content = content.replace(tag, html);
          }
        }
      }
      return content;
    },

    getEnterElement: function (editor) {
      switch (editor.config.enterMode) {
        case CKEDITOR.ENTER_P:
          return editor.document.createElement('p');

        case CKEDITOR.ENTER_DIV:
          return editor.document.createElement('div');
      }

      return editor.document.createElement('br');
    }
  };

  CKEDITOR.plugins.add('nodeasset', {
      lang: ['en'],
      requires: ['htmlwriter', 'iframedialog'],

      // Callbacks.
      replaceNodeAsset: function (tag_id, tag) {
        if (NodeAssets.outdated) {
          $.ajax({
            type: "POST",
            url: Drupal.settings.basePath + 'admin/node-assets/get/' + tag_id,
            data: {
              tag: tag
            },
            async: false,
            success: function (node_asset_content) {
              if (typeof(node_asset_content) == null) {
                node_asset_content = '';
              }

              var el = NodeAssets.outdated, container = NodeAssets.getContainer(tag_id, tag, node_asset_content),
                html = container.innerHTML;
              NodeAssets.outdated = null;

              if (html) {
                tagCache[tag_id] = {tag: tag, html: html};
                el.getParent() && el.$.parentNode.replaceChild(container.firstChild, el.$);
              }
              container.innerHTML = '';
            }
          });
        }
      },

      init: function (editor) {
        var path = this.path;

        // Re-attach styles on any editor mode switch to wysiwyg.
        editor.on('mode', function (evt) {
          var editor = evt.editor;
          if (editor.mode == 'wysiwyg') {
            editor.document.appendStyleSheet(path + 'node-assets-editor.css');
          }
        });

        // CKeditor instanceReady event.
        editor.on('instanceReady', function (evt) {
          var editor = evt.editor;
          if (CKEDITOR.instances && CKEDITOR.env) {
            // For webkit set cursor of wysiwyg to the end to prevent node asset in node asset pasting.
            if (CKEDITOR.env.webkit) {
              // Handle case for CKEditor 4.
              if (NodeAssets.getCKeditorVersion() >= 4) {
                // Create a range for the entire contents of the editor document body.
                var range = editor.createRange();
                // Move to the end of the range.
                range.moveToPosition(range.root, CKEDITOR.POSITION_BEFORE_END);
                // Putting the current selection there.
                editor.getSelection().selectRanges([range]);
              }
            }

            // Fix for CKEditor 3 & Chrome and for CKEditor 4 & FF.
            if ((NodeAssets.getCKeditorVersion() < 4 && CKEDITOR.env.webkit)
              || (NodeAssets.getCKeditorVersion() >= 4 && CKEDITOR.env.gecko)) {

              // Getting selection.
              var selected = editor.getSelection();
              // Getting ranges.
              var selected_ranges = selected.getRanges();
              // Selecting the starting node.
              var range = selected_ranges[0];

              if (range) {
                var node = range.startContainer;
                var parents = node.getParents(true);

                node = parents[parents.length - 2].getFirst();
                if (node) {
                  while (true) {
                    var x = node ? node.getNext() : null;

                    if (x == null) {
                      break;
                    }

                    node = x;
                  }

                  selected.selectElement(node);
                }

                selected_ranges = selected.getRanges();
                // False collapses the range to the end of the selected node, true before the node.
                selected_ranges[0].collapse(false);
                // Putting the current selection there.
                selected.selectRanges(selected_ranges);
              }
            }
          }
        });

        editor.on('key', function (evt) {
          var editor = evt.editor;

          // Backspace OR Delete.
          var keyCode = evt.data.keyCode, isHandled;

          if (keyCode in {8: 1, 46: 1}) {
            var sel = editor.getSelection(),
              selected,
              range = sel.getRanges()[0],
              rtl = (keyCode == 8), element, suggestedElement;

            if (!range) {
              return true;
            }

            // Remove the entire asset on fully selected content.
            if (NodeAssets.selectedElement) {
              element = NodeAssets.selectedElement;
            }
            else if (range.collapsed && range.startContainer) {
              var isNotWhitespace = CKEDITOR.dom.walker.whitespaces(true);
              // Handle the following special cases:
              // If we are on text node, but it contains only zero-width space (&#8203;).
              if (range.startContainer.type === CKEDITOR.NODE_TEXT && (text = range.startContainer.getText())
                && text.length === 1 && text.charCodeAt() === 8203
                && (suggestedElement = range.startContainer[rtl ? 'getPrevious' : 'getNext'](isNotWhitespace))
                && suggestedElement.$.attributes && suggestedElement.$.attributes['data-node-asset-cid']) {
                element = suggestedElement;
              }
              // We pressing key within body - we are on empty line (no text node or etc).
              else if (range.startContainer.type === CKEDITOR.NODE_ELEMENT && range.startContainer.getName() == 'body'
                && (suggestedElement = range.startContainer.getChild(rtl ? range.startOffset - 1 : range.startOffset + 1))
                && suggestedElement.$.attributes && suggestedElement.$.attributes['data-node-asset-cid']) {
                element = suggestedElement;
              }
            }

            if (element) {
              editor.fire('saveSnapshot');
              range.moveToPosition(element, rtl ? CKEDITOR.POSITION_BEFORE_START : CKEDITOR.POSITION_AFTER_END);
              element.remove();
              range.select();
              editor.fire('saveSnapshot');

              NodeAssets.deselect();

              isHandled = 1;
            }
          }

          return !isHandled;
        });

        // Wrapper for contentDom group events.
        editor.on('contentDom', function (evt) {
          editor.document.on('click', function (evt) {
            var element = evt.data.getTarget();
            var original_element = element;

            while (element && !(element.type === CKEDITOR.NODE_ELEMENT && element.data('node-asset-cid'))) {
              element = element.getParent();
            }

            if (element) {
              var selection = editor.getSelection();
              var range = new CKEDITOR.dom.range(selection.root);
              range.setStartBefore(element);
              range.setEndAfter(element);
              range.shrink(CKEDITOR.SHRINK_TEXT);
              selection.selectRanges([range]);

              NodeAssets.select(element);
              if (original_element.is('img')) {
                evt.data.preventDefault(true);
              }
            }
            else {
              NodeAssets.deselect();
            }
          });

          editor.document.on('mousedown', function (evt) {
            var element = evt.data.getTarget();

            if (element.is('img')) {
              while (element && !(element.type === CKEDITOR.NODE_ELEMENT && element.data('node-asset-cid'))) {
                element = element.getParent();
              }
              if (element) {
                evt.data.preventDefault(true);
              }
            }
          });
        });

        // Paste event.
        editor.on('paste', function (evt) {
          var data = evt.data, dataProcessor = new pasteProcessor(), htmlFilter = dataProcessor.htmlFilter,
            processed = {};

          htmlFilter.addRules({
            elements: {
              'div': function (element) {
                var wrapper, tagId, tag_id;
                NodeAssets.deselect(element);

                if (element.attributes && element.attributes['data-node-asset-cid']) {
                  // @todo: Check for webkit this functionality is forbidden.
                  if (CKEDITOR.env.webkit) {
                    return false;
                  }

                  tag_id = element.attributes['data-node-asset-cid'];

                  if (!processed[tag_id]) {
                    tagId = NodeAssets.generateId(tag_id);

                    if (typeof(tagCache[tag_id]) === 'undefined') {
                      NodeAssets.getDataById(tagId);
                    }

                    processed[tagId] = 1;
                    wrapper = new CKEDITOR.htmlParser.fragment.fromHtml(tagCache[tagId].html);

                    return wrapper.children[0];
                  }
                }
                return element;
              }
            }
          });

          if (typeof data['html'] !== 'undefined') {
            try {
              data['html'] = dataProcessor.toHtml(data['html']);
            }
            catch (e) {
              if (typeof(console) !== 'undefined') {
                console.log(NodeAssets.getTranslatedString(editor, 'node_assets_error_paste'));
              }
            }
          }
          NodeAssets.deselect();
        }, this);

        // Double click event.
        editor.on('doubleclick', function (evt) {
          var editor = evt.editor;

          // Getting selection.
          var element = NodeAssets.getSelected(editor), tag_id, tag, src;

          // Open dialog frame.
          if (element) {
            NodeAssets.outdated = element;
            tag_id = element.data('node-asset-cid');
            tag = encodeURIComponent(tagCache[tag_id].tag);
            src = Drupal.settings.basePath + 'admin/node-assets/override?render=popup&tag=' + tag;
            NodeAssets.openDialog(editor, 'node_asset_' + NodeAssets.parseId(tag_id, 'type'), src, element);
          }
        });

        this.NodeAssets = NodeAssets;

        var conf = Drupal.settings.ckeditor.plugins.nodeasset, nodeAssetType, type, execFn;
        if (!conf) {
          return;
        }

        for (nodeAssetType in conf) {
          if (conf.hasOwnProperty(nodeAssetType)) {
            type = 'node_asset_' + nodeAssetType;
            CKEDITOR.dialog.add(type, NodeAssets.dialog(editor, type));

            execFn = function (nodeAssetType) {
              return function (editor) {
                var urlType = nodeAssetType.replace('node_asset_', '');
                NodeAssets.openDialog(editor, nodeAssetType, Drupal.settings.basePath + 'node/add/' + urlType + '/?render=popup', null);
              };
            };

            editor.addCommand(type, {
              exec: execFn(type),
              canUndo: false,
              editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
            });

            editor.ui.addButton && editor.ui.addButton(type, {
              label: conf[nodeAssetType].name,
              command: type,
              icon: this.path + 'buttons/' + conf[nodeAssetType].icon
            });
          }
        }

        editor.addCommand('addLineAfter', {
          exec: function (editor) {
            var node = NodeAssets.getSelected(editor), newline;

            if (node) {
              NodeAssets.deselect();
              newline = NodeAssets.getEnterElement(editor);
              newline.insertAfter(node);
              editor.getSelection().selectElement(newline);
            }
          },
          canUndo: true
        });

        editor.addCommand('addLineBefore', {
          exec: function (editor) {
            var node = NodeAssets.getSelected(editor), newline;

            if (node) {
              NodeAssets.deselect();
              newline = NodeAssets.getEnterElement(editor);
              newline.insertBefore(node);
              editor.getSelection().selectElement(newline);
            }
          },
          canUndo: true
        });

        CKEDITOR.dialog.add('nodeAssetSearch', NodeAssets.searchDialog);
        editor.addCommand('nodeAssetSearch', new CKEDITOR.dialogCommand('nodeAssetSearch'));
        editor.ui.addButton && editor.ui.addButton('nodeAssetSearch', {
          label: NodeAssets.getTranslatedString(editor, 'node_assets_btn_search'),
          command: 'nodeAssetSearch',
          icon: this.path + 'search.png'
        });

        editor.addCommand('nodeAssetOverride', {
          exec: function (editor) {
            var element = NodeAssets.getSelected(editor), tag_id, tag, src;
            if (element) {
              NodeAssets.outdated = element;
              tag_id = element.data('node-asset-cid');
              tag = encodeURIComponent(tagCache[tag_id].tag);
              src = Drupal.settings.basePath + 'admin/node-assets/override?render=popup&tag=' + tag;
              NodeAssets.openDialog(editor, 'node_asset_' + NodeAssets.parseId(tag_id, 'type'), src, element);
            }
          },
          canUndo: false,
          editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
        });

        editor.addCommand('nodeAssetEdit', {
          exec: function (editor) {
            var element = NodeAssets.getSelected(editor);
            if (element) {
              NodeAssets.outdated = element;

              var tag_id = element.data('node-asset-cid');
              var params = NodeAssets.getTagData(tagCache[tag_id].tag);
              var src = [
                Drupal.settings.basePath + 'node-assets-uuid',
                'node',
                params.uuid,
                'edit',
                '?render=popup&mode=' + params.mode,
              ].join('/');

              NodeAssets.openDialog(editor, 'node_asset_' + NodeAssets.parseId(tag_id, 'type'), src, element);
            }
          },
          canUndo: false,
          editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
        });

        editor.addCommand('nodeAssetDelete', {
          exec: function (editor) {
            NodeAssets.removeAssetFromEditor(editor, NodeAssets.getSelected(editor));
          },
          canUndo: true,
          editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
        });

        editor.addCommand('nodeAssetCut', {
          exec: function (editor) {
            NodeAssets.removeAssetFromEditor(editor, NodeAssets.getSelected(editor), true);
          },
          canUndo: true,
          editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
        });

        editor.addCommand('nodeAssetPaste', {
          exec: function (editor) {
            if (cutted !== null) {
              NodeAssets.deselect(cutted);
              editor.insertElement(cutted);
              cutted = null;
            }
          },
          canUndo: true,
          editorFocus: CKEDITOR.env.ie || CKEDITOR.env.webkit
        });

        // Create context menu.
        if (editor.addMenuItem) {
          editor.addMenuGroup('node_asset_operations');

          editor.addMenuItems({
            nodeassetcut: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_cut'),
              command: 'nodeAssetCut',
              group: 'node_asset_operations',
              icon: this.path + 'cut.png',
            },
            nodeassetpaste: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_paste'),
              command: 'nodeAssetPaste',
              group: 'node_asset_operations',
              icon: this.path + 'paste.png',
            },
            nodeassetdelete: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_delete'),
              command: 'nodeAssetDelete',
              group: 'node_asset_operations',
              icon: this.path + 'delete.png',
            }
          });

          editor.addMenuGroup('node_asset_newline', 200);
          editor.addMenuItems({
            addLineBefore: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_nl_before'),
              command: 'addLineBefore',
              group: 'node_asset_newline',
              order: 1
            },
            addLineAfter: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_nl_after'),
              command: 'addLineAfter',
              group: 'node_asset_newline',
              order: 2
            }
          });

          editor.addMenuGroup('node_asset_edit');
          editor.addMenuItems({
            nodeassetoverride: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_override'),
              command: 'nodeAssetOverride',
              group: 'node_asset_edit',
              icon: this.path + 'override.png',
            },
            nodeassetedit: {
              label: NodeAssets.getTranslatedString(editor, 'node_assets_edit'),
              command: 'nodeAssetEdit',
              group: 'node_asset_edit',
              icon: this.path + 'edit.png',
            }
          });
        }

        if (editor.contextMenu) {
          editor.contextMenu.addListener(function (element, selection) {
            var type, conf, menu = {};
            var editor = selection.root.editor;

            while (element && !(element.type === CKEDITOR.NODE_ELEMENT && element.data('node-asset-cid'))) {
              element = element.getParent();
            }

            // Open context menu.
            if (element) {
              // If we are on node asset with contextual menu - deny any other options like paste and etc.
              editor.contextMenu.removeAll();

              // Select node asset element, if element wasn't selected before.
              if (!element.hasClass('selected')) {
                NodeAssets.select(element)
              }

              type = NodeAssets.parseId(element.data('node-asset-cid'), 'type');
              conf = Drupal.settings.ckeditor.plugins.nodeasset[type];

              // We just control item visibility, real access check will be on server side.
              menu.nodeassetedit = CKEDITOR.TRISTATE_ON;

              if (conf && conf.modes && !(conf.modes.length === 1 && conf.modes.full && !conf.fields.length)) {
                menu.nodeassetoverride = CKEDITOR.TRISTATE_ON;
              }

              menu.nodeassetdelete = CKEDITOR.TRISTATE_ON;
              menu.nodeassetcut = CKEDITOR.TRISTATE_ON;
              menu.addLineBefore = CKEDITOR.TRISTATE_ON;
              menu.addLineAfter = CKEDITOR.TRISTATE_ON;

            }
            else {
              if (cutted !== null) {
                menu = {nodeassetpaste: CKEDITOR.TRISTATE_ON};
              }
            }

            return menu;
          });
        }

        // The paste processor here is just a reduced copy of html data processor.
        var pasteProcessor = function () {
          this.htmlFilter = new CKEDITOR.htmlParser.filter();
        };

        pasteProcessor.prototype = {
          toHtml: function (data) {
            var fragment = CKEDITOR.htmlParser.fragment.fromHtml(data, false),
              writer = new CKEDITOR.htmlParser.basicWriter();

            fragment.writeHtml(writer, this.htmlFilter);
            return writer.getHtml(true);
          }
        };
      },

      afterInit: function (editor) {
        // Register a filter to displaying placeholders after mode change.
        var dataProcessor = editor.dataProcessor,
          dataFilter = dataProcessor && dataProcessor.dataFilter,
          htmlFilter = dataProcessor && dataProcessor.htmlFilter,
          HtmlDPtoHtml = dataProcessor && editor.dataProcessor.toHtml;

        if (HtmlDPtoHtml) {
          // Unprotect some flash tags, force democracy.
          editor.dataProcessor.toHtml = function (data, fixForBody) {

            var unprotectFlashElementNamesRegex = /(<\/?)cke:((?:object|embed|param)[^>]*>)/gi;
            data = HtmlDPtoHtml.apply(editor.dataProcessor, [data, fixForBody]);

            return data.replace(unprotectFlashElementNamesRegex, '$1$2');
          };
        }

        if (dataFilter) {
          dataFilter.addRules({
            text: function (text) {
              result = NodeAssets.attach(text);
              return result;
            }
          });
        }

        if (htmlFilter) {
          htmlFilter.addRules({
              elements: {
                'div': function (element) {
                  if (element.attributes && element.attributes['data-node-asset-cid']) {
                    var tag_id = element.attributes['data-node-asset-cid'];

                    var tag = tagCache[tag_id].tag;
                    tag = tag.replace(/</g, '&lt;');
                    tag = tag.replace(/>/g, '&gt;');

                    var tagEl = new CKEDITOR.htmlParser.fragment.fromHtml(tag);
                    return tagEl.children[0];
                  }

                  return element;
                }
              }
            },
            {
              applyToAll: true
            });
        }
      }
    }
  );
})(jQuery);
